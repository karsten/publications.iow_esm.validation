%\documentclass[gmd, manuscript]{copernicus}
\documentclass[gmd]{copernicus}

% To be removed be fore submission
\usepackage{lipsum}
\newcommand{\open}[1]{\textcolor{red}{#1}}
%

\begin{document}

\title{Validation of the IOW Earth System Model (version 1.04.00) for the Baltic Sea region}


\Author[1]{Sven}{Karsten}
\Author[1]{H.~E.~Markus}{Meier}
\Author[1]{Matthias}{Gr\"oger}
\Author[1]{Hagen}{Radtke}
%\Author[2]{Ha~T.~M.}{Ho-Hagemann}
\Author[1]{Hossein}{Mashayekh}
%\Author[1]{Thomas}{Neumann}
\affil[1]{Leibniz Institute for Baltic Sea Research Warnem\"unde (IOW), Seestra{\ss}e 15, 18119 Rostock, Germany}
%\affil[2]{Helmholtz-Zentrum Hereon, Max-Planck-Stra{\ss}e 1, 21502 Geesthacht, Germany}


\correspondence{Sven Karsten (sven.karsten@io-warnemuende.de)}

\runningtitle{Validation of the IOW Earth System Model}

\runningauthor{S. Karsten et al.}

\received{}
\pubdiscuss{} %% only important for two-stage journals
\revised{}
\accepted{}
\published{}

%% These dates will be inserted by Copernicus Publications during the typesetting process.


\firstpage{1}

\maketitle



\begin{abstract}
\open{\lipsum[1]}
\end{abstract}

\introduction

Projections for the Earth's future climate are based on global ESMs, i.e. platforms that interactively couple different components of the earth system (e.g. atmosphere, biosphere, cryosphere ocean, e.g.~\citep{heinze2019}. 
%
Still, the resolution of these models is insufficient to explicitly resolve important small scale processes (e.g. convection, turbulence, etc) which hampers their application for regional environmental issues like e.g. marine spatial planning, or local climate adaption and mitigation (e.g.~\citep{giorgi2015}). 
%
Therefore regional climate models were developed which represent a step forward to more sophistically include small scale processes and more realistically represent topography. 
%
However, the majority of these models consists only of a single standalone model for the atmosphere that is driven by input data either from global models or reanalysis products at the model boundaries. 
%
For future projections, this approach is problematic as input information can only be derived from global models which can have substantial biases in the region of interest. 
%
While numerous tested methods exist to bias-correct model forcing data for the historical period (e.g. \citep{TEUTSCHBEIN201212, vaittinada2021ensemble}) their application for future periods can not be validated and may be therefore problematic. 
%
In addition, it was shown that uncoupled ocean standalone models are too tightly controlled by the atmospheric model in future projections (Mathis et al., 2018). 
%
This argues for the development of regional coupled ocean-atmosphere models for future projections. 

For the Baltic Sea early attempts to develop regional high resolution atmosphere-Baltic Sea models go back to the late 90ies but suffered under strong model drifts rapidity leading to unrealistic solutions already after one year (Gustaffsson et al., 1998). 
%
However, growing computational resources promoted the developments of coupled models capable of performing simulations over several decades (e.g. Do\"scher et al., 2002; Meier and Do\"scher, 2002; Kjellstro\"m et al., 2005).
%
\open{Markus may summarize the main findings from these studies...}
%
In the following decades several coupled ocean-atmosphere models were developed for a manyfold of purposes and with varying coupling domains (see table 1 in Gro\"ger et al, 2021 for an overview).

In the uncoupled mode, the solution of an ocean model is relaxed to the atmospheric thermal state which is usually based on reanalysis data of comparably good quality. 
%
By coupling, this observational constrained is replaced by a dynamic atmospheric circulation model. 
%
Thus, in the case of hindcast simulations, the added value of coupling is not expected to give more accurate e.g. SSTs but primarily provides a more realistic energy flow in the model. 
%
However, in cases where reanalysis data is of low quality coupled models can substantially improve SSTs. 
%
For the Baltic Sea two independent coupled model systems from the Danish Meteorological Institute and the Swedish Meteorological and Hydrological Institute demonstrated a significant improvement of simulated winter SSTs compared to their corresponding ocean only simulations (Tian et al., 2013, Gro\"ger et al., 2015). 
%
Further analysis revealed that the bias arose from too cold temperatures inherited from the ERA40 reanalysis product used for the forcing in the two model systems (i.e. ``garbage in, garbage out problem''). 
%
Gro\"ger et al. (2015) found that in the coupled model version a feedback loop involving atmospheric boundary layer dynamics and vertical ocean mixing lead to warmer SSTs in winter thereby reducing the cold biases in the coupled model version. 
%
Hence, in case atmospheric forcing is of low quality, which may be considered the standard assumption for global climate model output, coupled ocean atmosphere models are the preferred tool for regional climate projections for the Baltic Sea region (Gro\"ger et al., 2021a).

One of the main questions is how far the solution of a coupled ocean-atmosphere model model differs from their standalone component which certainly is dependent on the size of the coupled domain (i.e. the size of interactively coupled ocean domain). 
%
Large domains including not only the European marginal Sea (North Sea, Baltic Sea) but also the Mediterranean have demonstrate large effects on atmospheric climate variables over continental scales far remote from the coupling area (\citep{akhtar2014medicanes}Primo et al., 2019; Keleman et al., 2019; Akhtar et al., 2019; Cabos et al., 2020). 
%
For smaller domains that restrict the coupling to only the North Sea and Baltic Sea the answer appears to be model dependent. 
%
Van Pham et al. (2014) demonstrated far reaching impacts on air temperatures over land in the coupled NEMO-CCLM model whereas Gro\"ger et al. (2015, 2021a, 2021b) found significant coupling effects over land restricted along the coasts using the coupled NEMO-RCA4 model.

Related to the coupling effect over land is the question whether or not coupled models provide different solutions in climate change projections than the traditional approach (atmosphere-only models) mainly pursued in the Euro-CORDEX framework. 
%
A systematic assessment was performed by Gro\"ger et al. (2021b) who showed that for most regions of European land climate, the mean changes are within the model uncertainty of traditional atmosphere standalone model ensembles. 
%
However, over maritime areas such as the coasts of the Baltic Sea, there is evidence that the coupling effect significantly alters the solution of climate extremes such as tropical nights, the number of frost days, or the number of warm days (Gro\"ger et al., 2021a; 2021b). 
%
In conclusion, for coastal regions coupled models likely yield a different solution than uncoupled models and in particular for climate extremes.

In order to produce robust assessments of climate change uncertainty requires consideration of uncertainties with respect to various sources. 
%
The European continent and its marginal seas are located between the polar climate zone in the north and subtropical climate in the south and is likewise influenced by temperated maritime climate in the west and continental climate with high seasonal amplitudes in the east and thus makes this region a challenge for the development of coupled ESMs~(Gro\"ger et al., 2021a).  
%
Consequently,  the climate of Europe is highly variable, resulting in many different climate zones to be distinguished (Ko\"ppen et al. 1923). 
%
This is in particular the case for the Baltic Sea region which is known for its high natural variability (Meier et al., 2021).

The CMIP, e.g. CMIP6~(Eyring et al., 2016), and the CORDEX within CMIP 6 (Giorgi et al., 2015; Gutowski et al., 2016) provide protocols for the harmonization of model forcing, validation and analysis of results and so allows a systematic assessment of future climate changes. 
%
For the Baltic Sea most recent assessment of climate change is given in the Baltic Earth assessment reports (e.g. Meier et al., 2022a; 2022b; Christensen et al., 2022) but is still based on the former CMIP 5 scenarios.
%
The here described IOW regional earth system model has been developed to dynamically downscale the latest version of climate change scenarios from the 6th phase of the CMIP.

\open{Here some history of the so far used components (MOM, CCLM) and why they are chosen to implement could follow...}


\section{Theoretical background and methodology}

\open{Recapitulate, summarize "Exchange-grid coupling approach for the IOW Earth System Model (version 1.04.00) of the Baltic Sea region"}

\section{Simulation and analysis setup}

In order to gain the first impression on the quality of the developed coupled ESM, we perform the comparison to its uncoupled counterparts as well as to the ERA5~\citep{era5} reanalysis data.
%
However, it should be stressed that this comparison is not the main focus of this manuscript and will be published elsewhere.
%
The focus of this article is on the flexibility in model development, the consistency of the presented flux calculation and the facilitation of running simulations as well as performing subsequent data analysis that is enabled by the presented framework. 
\open{Make this also clear in the end of the introduction...}

The following setup has been used for performing first benchmark simulation, see Fig.\,\ref{fig:simulation-setup} for illustration. 

\begin{figure*}
	\centering
    \includegraphics[width=0.8\textwidth]{"./figures/coupled_vs_uncoupled.pdf"}
    \captionsetup{width=\linewidth}
    \caption{\label{fig:simulation-setup}\textbf{Model setup for coupled and uncoupled runs.} Atmospheric grid has a resolution of 0.22 by 0.22$^\circ$ whereas the ocean model's grid has the size 3 by 3 nm.} 
\end{figure*}

\subsection{Uncoupled atmospheric model}
First, ERA5 reanalysis data is prepared as forcing/boundary data for the CCLM  atmospheric model for the time range 1959-01-01 - \open{2019-12-31}.
%
The forcing/boundary data have been processed using the COSMO pre-processing tool \textit{int2lm}~\citep{schattler2009}, which performs the interpolation from the coarse resolution ERA5 data to the employed resolution of the CCLM.
%
With these forcing files an \text{uncoupled} CCLM run is performed over the Euro-CORDEX~\citep{jacob2014eurocordex} domain using a resolution of \open{0.11$^\circ$ by 0.11$^\circ$}.

\subsection{Uncoupled ocean model}
\label{sec:uncoupled-MOM}

From the resulting atmospheric model output, a meteorological forcing for the MOM5 stand-alone ocean model for the Baltic Sea is generated.
%
The uncoupled MOM5 simulates the Baltic Sea model with a horizontal resolution of 3$ \times $3 nautical miles.
%
At the open boundary to the North Sea, we use climatologies for all prognostic model variable. 
%
The sea level elevation is estimated from the wind field by a statistical approach and the river runoff and nutrient loads are derived from HELCOM compilations~\citep{neumann2021}. 
%
The marine bio-geochemistry is modeled by the latest version of the internally coupled ERGOM~\citep{neumann2021}.


\subsection{Coupled ESM}
For the coupled ESM run the same atmospheric (ERA5) forcings is used as for the uncoupled CCLM run.
%
Likewise, for the ocean model the same OBCs and river runoff data has been used as for the uncoupled MOM5 run.
%
The other input parameters of the model components are also kept the same as in both, atmospheric and ocean model, uncoupled runs.
%
However, the interaction between ocean and atmosphere over the Baltic Sea region is now realized as a two-way coupling via the \textit{exchange grid} and the \textit{flux calculator}.
%
(Thus, the forcings generated from the uncoupled CCLM run are not needed for the coupled run.)\\

The data of all three kinds of runs, i.e. uncoupled CCLM, uncoupled MOM5 and the coupled ESM, is analyzed in the following for a time span of 60 years, i.e. 1960-01-01 - 2019-12-31.
%
The employed analysis procedure is described the following.


\subsection{Analysis protocol}

In order to judge the quality of the individual model runs the data has been compared to ERA5 reanalysis data.
%
This comparison has been implemented into an automatic post-processing procedure, with details given in Sect.\,\ref{sec:analysis-protocol}.

For the sake of brevity, we will restrict ourselves to show only seasonally averaged differences between the models as well as between the models and the ERA5 reference data.
%
A full analysis including a final validation will be done separately in future publications.


\section{Results}

\subsection{Coupled against uncoupled model results against reanalysis data}

First, the quality of the coupled model's output is investigated when using the most natural choice for an exchange grid, i.e. the intersection grid.
%
The seasonal anomaly of the SST with respect to the ERA5 reference data is shown in Fig.\,\ref{fig:MOM5-SST-coupled-vs-uncoupled}, upper row.
%
Whereas, in the lower row, the comparison is equally performed for the uncoupled (stand-alone) ocean model, as described in Sect.\,\ref{sec:uncoupled-MOM}.

\begin{figure*}
\centering
\includegraphics[width=0.99\textwidth]{"./data_figures/MOM5/SST-coupled-vs-uncoupled.png"}
\captionsetup{width=\linewidth}
\caption{\label{fig:MOM5-SST-coupled-vs-uncoupled}
\textbf{Comparison for SST simulated by coupled and uncoupled MOM5 ocean model.} 
%
The sign of the difference is such that the averaged reference SST is subtracted from the model one. 
%
The upper row corresponds to the fully coupled ocean model, whereas the lower row depicts the results of the stand-alone ocean model. 
%
The seasons correspond to the usual choice: spring (MAM), summer (JJA), fall (SON) and winter (DJF). 
%
The total time average is labeled by mean} 
\end{figure*}
%
One can see that the coupled ocean model features a cold bias that is especially strong in the summer season.
%
For instance, in the Gulf of Bothnia, the simulated SST obtained from the coupled model is too low by more than 4$^\circ$C.
%
Although the uncoupled model feature a cold bias in summer as well, its magnitude is restricted to slightly more than 2$^\circ$C in a much smaller region.
%
In the other seasons one can see the same situation but with smaller proportions, e.g. in winter both models nearly yield the same SST.

This cold bias becomes also apparent if one considers the two-meter air temperature, see Figs.\,{\ref{fig:CCLM-T2M-coupled-vs-uncoupled} and \ref{fig:CCLM-T2M-coupled-vs-uncoupled-compare}}.
%
In the coupled domain, i.e. the Baltic Sea, there is stronger cold bias visible for the coupled atmospheric model than for its uncoupled counterpart.
%
The highest magnitude of this deviation is again present during the summer season, which becomes especially obvious when considering the direct comparison between the two models in Fig.\,\ref{fig:CCLM-T2M-coupled-vs-uncoupled-compare}.

We believe that this cold bias stems from an inaccuracy in some of the exchanged fluxes.
%
The fact that the coupled model is much more effected by such a deficiency can be explained by a simple model consideration as described in Sect.\,\ref{sec:appendix-simple-model}.
%


\begin{figure*}
	\centering
    \includegraphics[width=0.99\textwidth]{"./data_figures/CCLM/T_2M_AV-coupled-vs-uncoupled.png"}
    \captionsetup{width=\linewidth}
    \caption{\label{fig:CCLM-T2M-coupled-vs-uncoupled}\textbf{Comparison for averaged two-meter air temperature (\texttt{T\_2M\_AV}) simulated by coupled and uncoupled CCLM atmospheric model.}} 
\end{figure*}

\begin{figure*}
	\centering
    \includegraphics[width=0.99\textwidth]{"./data_figures/CCLM/T_2M_AV-coupled-vs-uncoupled-compare.png"}
    \captionsetup{width=\linewidth}
    \caption{\label{fig:CCLM-T2M-coupled-vs-uncoupled-compare}\textbf{Direct comparison for averaged two-meter air temperature (\texttt{T\_2M\_AV}) between coupled and uncoupled CCLM atmospheric model.} } 
\end{figure*}


\subsection{Results of the bias-corrected coupled model}

\open{
\begin{itemize}
\item show reduced seasonal temperature anomalies with respect to ERA5, E-OBS (for CCLM)
\item show improved fluxes with respect to ERA5
\item show vertical temperature, salinity profiles compared to BED (or Lev's combined dataset?) 
\end{itemize}
}


\subsection{\open{Application to interesting scientific question}}
\open{
Let's discuss it and fix a nice topic.
}


\conclusions  %% \conclusions[modified heading if necessary]

\open{\lipsum}

%% The following commands are for the statements about the availability of data sets and/or software code corresponding to the manuscript.
%% It is strongly recommended to make use of these sections in case data sets and/or software code have been part of your research the article is based on.

\codeavailability{TEXT} %% use this section when having only software code available


\dataavailability{TEXT} %% use this section when having only data sets available


\codedataavailability{TEXT} %% use this section when having data sets and software code available


\sampleavailability{TEXT} %% use this section when having geoscientific samples available


\videosupplement{TEXT} %% use this section when having video supplements available


\appendix

\section{Simple model for stronger cold-bias in the coupled model simulations}


In order to explain the strong cold bias during summer time in the coupled model simulation, a simplistic model is considered.


\subsection{The simplistic model}
\label{sec:appendix-simple-model}

The atmospheric temperature directly above the water surface $T_a$ and the water-surface temperature $T_w$ are written as functions of a set of quantities, i.e.
%
\begin{align}
T_a = T_a(T_w, \{A_i\}), \qquad T_w = T_w(T_a, \phi_{\mathrm{sw}}, \phi_{\mathrm{lh}}, \{W_i\}),
\end{align}
%
where $\{A_i\}$ and $\{W_i\}$ are all DOFs in the atmosphere and the water, respectively, that are not of interest now.
%
The downward shortwave radiation flux that heats up the ocean is explicitly taken into account via $\phi_{\mathrm{sw}}$ as well as the latent heat flux $\phi_{\mathrm{lh}}$ that mainly cools the ocean.
%
The total change of these quantities can then be written down as
\begin{align}
\mathrm{d} T_a = \frac{ \partial T_a}{ T_w} \mathrm{d} T_w + \sum_{i} \frac{ \partial T_a}{ A_i} \mathrm{d} A_i \\
\mathrm{d} T_w = \frac{ \partial T_w}{ T_a} \mathrm{d} T_a + \frac{\partial T_w}{\partial \phi_{\mathrm{sw}}} \mathrm{d} \phi_{\mathrm{sw}} + \frac{\partial T_w}{\partial \phi_{\mathrm{lh}}} \mathrm{d} \phi_{\mathrm{lh}} + \sum_{i} \frac{\partial T_w}{\partial W_i} \mathrm{d} W_i.
\end{align}

Now we want to construct a simplistic scenario, where all the unconsidered DOFs are fixed, i.e. 
%
\begin{align}
\mathrm{d} W_i = \mathrm{d} A_i = 0
\end{align}
%
We then get
%
\begin{align}
\mathrm{d} T_a = \frac{ \partial T_a}{ \partial T_w} \mathrm{d} T_w  \\
\mathrm{d} T_w = \frac{ \partial T_w}{ \partial T_a} \mathrm{d} T_a + \frac{\partial T_w}{\partial \phi_{\mathrm{sw}}} \mathrm{d} \phi_{\mathrm{sw}} + \frac{\partial T_w}{\partial \phi_{\mathrm{lh}}} \mathrm{d} \phi_{\mathrm{lh}}.
\end{align}

We may now make some intuitive assumptions.
%
First, we assume that 
\begin{align}
\frac{ \partial T_a}{ \partial T_w} =: \alpha \geq 0  \\
\frac{ \partial T_w}{ \partial T_a} =: \beta \geq 0.
\end{align}
which basically states that if the atmosphere becomes warmer the ocean will likely become warmer as well and vice versa. 
%
Practically, this mutual heating is simply mediated by sensible heat and thermal radiation fluxes.
%
In a similar fashion we assume for the shortwave radiation that it is mainly heating the ocean, i.e.
%
\begin{align}
\frac{ \partial T_w}{ \partial \phi_{\mathrm{sw}}} =: \gamma \geq 0.
\end{align}
%
and that the latent heat flux is cooling the ocean
%
\begin{align}
\frac{ \partial T_w}{ \phi_{\mathrm{lh}}} =: \delta \leq 0,
\end{align}
%
since we have mainly evaporation from the ocean to the atmosphere.
%
Plugging these assumptions into the above equations we get
%
\begin{align}
\mathrm{d} T_a = \alpha \mathrm{d} T_w  \\
\mathrm{d} T_w = \beta \mathrm{d} T_a + \gamma \mathrm{d} \phi_{\mathrm{sw}} + \delta \mathrm{d} \phi_{\mathrm{lh}}.
\end{align}
%
Now we insert the upper into the lower equation to obtain a closed formula for the water's temperature change, i.e.
%
\begin{align}
\mathrm{d} T_w = \alpha \beta  \mathrm{d} T_w + \gamma \mathrm{d} \phi_{\mathrm{sw}} + \delta \mathrm{d} \phi_{\mathrm{lh}}.
\end{align}
%
Obvious rearranging yields 
%
\begin{align}
\mathrm{d} T_w = \frac{1}{1- \alpha \beta } (\gamma \mathrm{d} \phi_{\mathrm{sw}} + \delta \mathrm{d} \phi_{\mathrm{lh}})
\end{align}
%
which states that, in our simplistic scenario, the change of the water's temperature is proportional to the change in shortwave radiation flux.


\subsection{Comparing coupled and uncoupled cases}

In the following we want to compare the cases when the ocean and the atmosphere are coupled or uncoupled in our simplistic model.

First we consider the uncoupled case.
%
Here we simply have 
\begin{align}
\alpha = \frac{\partial T_a}{\partial T_w} \equiv 0
\end{align}
%
(and thus $\alpha \beta = 0$), which means that there is no feedback from the ocean to the atmosphere.
%
In that case the change of the water's temperature is simply given by
%
\begin{align}
\mathrm{d} T_w = \gamma \mathrm{d} \phi_{\mathrm{sw}}  + \delta \mathrm{d} \phi_{\mathrm{lh}}
\end{align}
%

Second, we consider the coupled case.
%
{\color{red} Here we can assume that
\begin{align}
0 < \alpha < 1 \\
0 < \beta < 1
\end{align}
}
In that case we have
%
\begin{align}
\mathrm{d} T_w =\tilde{\gamma} \mathrm{d} \phi_{\mathrm{sw}} + \tilde{\delta} \mathrm{d} \phi_{\mathrm{lh}}
\end{align}
%
with 
%
\begin{align}
\tilde{\gamma} := \frac{\gamma}{1- \alpha \beta }, \qquad \tilde{\delta} := \frac{\delta}{1- \alpha \beta }.
\end{align}
%
{\color{red} Since $0<\alpha<1$ and $0<\beta<1$} we have 
%
\begin{align}
\tilde{\gamma} > \gamma, \qquad \tilde{\delta} < \delta.
\end{align}
% 
The different signs are due to the assumption $\gamma > 0$ and $\delta < 0$.
%
Consequently, in the coupled case the impact of a changing shortwave radiation and latent heat flux on the water's temperature is \textit{amplified} with respect to the uncoupled case.
%
Thus, reducing the incoming shortwave radiation ($\mathrm{d} \phi_{\mathrm{sw}} < 0$) and increasing the latent heat flux ($\mathrm{d} \phi_{\mathrm{lh}} > 0$) we decrease the water's temperature more strongly in the coupled case.
%
Hence, a deficit in the radiation flux and an overestimated latent heat (as it is present for CCLM) leads to a stronger cold bias for the coupled model.

\section{Analysis protocol}
\label{sec:analysis-protocol}

As stated in the main text, the comparison to reference data has been implemented into an automatic post-processing procedure.
%
The data flow in this procedure is depicted in Fig.\,\ref{fig:flow-diagram} and sketched in the following.
%
\begin{figure*}
	\centering
    \includegraphics[width=0.8\textwidth]{"./figures/flow-diagram.pdf"}
    \captionsetup{width=\linewidth}
    \caption{\label{fig:flow-diagram}\textbf{Diagram for the data flow during automatized validation procedure.} The main data flow is from left to right and is exemplified for the ocean model's variable \texttt{SST} (sea surface temperature). The arrows depict the particular flow of the data from one validation task to the next. The raw model/reference data (left column) is first pre-processed into a generic format (right column) and subsequently analyzed by CDO operations (upper row). The figures are then plotted by Python tools (lower row) and compiled into a validation report (bottom).} 
\end{figure*}

The reasoning of of the procedure is as follows.
%
First, the raw data of one model within the ESM as well as the chosen reference data are pre-processed such that all considered model variables are stored in files with the name of the model variable.
%
For instance the MOM variable \texttt{SST} (sea surface temperature) is stored in file \texttt{SST.nc} that is the broadly used NetCDF~\citep{rew1990netcdf} format.
%
The same step is applied to the reference data, where usually the variable is renamed such that it coincides with model's variable name.
%
Additionally other transformations can be applied such as unit conversion, multiplication by a factor, etc.
%
Practically these steps are implemented as calls of the CDO~\citep{schulzweida_uwe_2019_2558193} tool and can be configured via a configuration file that is described below.
%
In order to calculate differences between model and reference data, the latter is automatically remapped onto the model's grid. 
%
Note that not only ERA5 but also any other data that can be transformed in the described way would be a suitable reference.
% 
Note further that the pre-processing might be very specific to the model/reference since usually different models/references feature different data formats.
%
However, after these very first pre-processing steps the model/reference data format is intended to be model-independent since only the variable's name is needed to perform the following analysis steps. 
%
This generic data format avoids code duplication since all of the analysis and the plotting scripts can then be equally applied to different models within the ESM having different reference data. 

The analysis and the plotting steps of validation procedure are configured in an own module implemented in the programming language Python~\citep{python3} via a so-called Python \textit{dictionary}.
%
Each model variable of interest is assigned to a key with the variable's name in that dictionary.
%
The value belonging to this key is a dictionary itself, that can contain the 
\begin{itemize}
\item seasons for temporal means, i.e. a list months to be averaged over
\item stations defined by coordinates
\item regions defined by coordinates of a rectangle or a mask file that cuts the particular region from the data 
\item temporal mean operations that can be applied to time series data and vertical profiles that are extracted for the stations and regions
\item file pattern to the reference data files that contain the corresponding reference variable 
\item configuration for the plotting
\item long name and description of the variable
\end{itemize} 

The analysis is performed according to the configuration by Python scripts that mainly call CDO routines.
%
In addition Taylor diagrams and cost functions are calculated for the time series data.
%
Importantly, all interim data that is produced during the analysis is stored in NetCDF files to ensure reproducibility of the figures.
%
Subsequently to the analysis, the results are plotted with the help of various Python libraries.
%
The actual plotting script for each figure is provided to the user in order to allow later customizations.
%
The plots are then compiled into a single report that is either provided as a Markdown~\citep{markdown-guide} file, an interactive Jupyter Notebook~\citep{kluyver2016jupyter} or as ready-to-read PDF file.
%
The Jupyter Notebook format enables the user to adapt the individual plotting scripts to customize figures to be used in publications or other documents.
%
An example of two validation reports in the PDF format can be found in the \open{manual}.

\noappendix       %% use this to mark the end of the appendix section. Otherwise the figures might be numbered incorrectly (e.g. 10 instead of 1).

%% Regarding figures and tables in appendices, the following two options are possible depending on your general handling of figures and tables in the manuscript environment:

%% Option 1: If you sorted all figures and tables into the sections of the text, please also sort the appendix figures and appendix tables into the respective appendix sections.
%% They will be correctly named automatically.

%% Option 2: If you put all figures after the reference list, please insert appendix tables and figures after the normal tables and figures.
%% To rename them correctly to A1, A2, etc., please add the following commands in front of them:

\appendixfigures  %% needs to be added in front of appendix figures

\appendixtables   %% needs to be added in front of appendix tables

%% Please add \clearpage between each table and/or figure. Further guidelines on figures and tables can be found below.



\authorcontribution{TEXT} %% this section is mandatory

\competinginterests{TEXT} %% this section is mandatory even if you declare that no competing interests are present

\disclaimer{TEXT} %% optional section

\begin{acknowledgements}
TEXT
\end{acknowledgements}




%% REFERENCES

%% The reference list is compiled as follows:

\bibliographystyle{copernicus}
\bibliography{all}

%% Since the Copernicus LaTeX package includes the BibTeX style file copernicus.bst,
%% authors experienced with BibTeX only have to include the following two lines:
%%
%% \bibliographystyle{copernicus}
%% \bibliography{example.bib}
%%
%% URLs and DOIs can be entered in your BibTeX file as:
%%
%% URL = {http://www.xyz.org/~jones/idx_g.htm}
%% DOI = {10.5194/xyz}


%% LITERATURE CITATIONS
%%
%% command                        & example result
%% \citet{jones90}|               & Jones et al. (1990)
%% \citep{jones90}|               & (Jones et al., 1990)
%% \citep{jones90,jones93}|       & (Jones et al., 1990, 1993)
%% \citep[p.~32]{jones90}|        & (Jones et al., 1990, p.~32)
%% \citep[e.g.,][]{jones90}|      & (e.g., Jones et al., 1990)
%% \citep[e.g.,][p.~32]{jones90}| & (e.g., Jones et al., 1990, p.~32)
%% \citeauthor{jones90}|          & Jones et al.
%% \citeyear{jones90}|            & 1990



%% FIGURES

%% When figures and tables are placed at the end of the MS (article in one-column style), please add \clearpage
%% between bibliography and first table and/or figure as well as between each table and/or figure.

% The figure files should be labelled correctly with Arabic numerals (e.g. fig01.jpg, fig02.png).


%% ONE-COLUMN FIGURES

%%f
%\begin{figure}[t]
%\includegraphics[width=8.3cm]{FILE NAME}
%\caption{TEXT}
%\end{figure}
%
%%% TWO-COLUMN FIGURES
%
%%f
%\begin{figure*}[t]
%\includegraphics[width=12cm]{FILE NAME}
%\caption{TEXT}
%\end{figure*}
%
%
%%% TABLES
%%%
%%% The different columns must be seperated with a & command and should
%%% end with \\ to identify the column brake.
%
%%% ONE-COLUMN TABLE
%
%%t
%\begin{table}[t]
%\caption{TEXT}
%\begin{tabular}{column = lcr}
%\tophline
%
%\middlehline
%
%\bottomhline
%\end{tabular}
%\belowtable{} % Table Footnotes
%\end{table}
%
%%% TWO-COLUMN TABLE
%
%%t
%\begin{table*}[t]
%\caption{TEXT}
%\begin{tabular}{column = lcr}
%\tophline
%
%\middlehline
%
%\bottomhline
%\end{tabular}
%\belowtable{} % Table Footnotes
%\end{table*}
%
%%% LANDSCAPE TABLE
%
%%t
%\begin{sidewaystable*}[t]
%\caption{TEXT}
%\begin{tabular}{column = lcr}
%\tophline
%
%\middlehline
%
%\bottomhline
%\end{tabular}
%\belowtable{} % Table Footnotes
%\end{sidewaystable*}
%
%
%%% MATHEMATICAL EXPRESSIONS
%
%%% All papers typeset by Copernicus Publications follow the math typesetting regulations
%%% given by the IUPAC Green Book (IUPAC: Quantities, Units and Symbols in Physical Chemistry,
%%% 2nd Edn., Blackwell Science, available at: http://old.iupac.org/publications/books/gbook/green_book_2ed.pdf, 1993).
%%%
%%% Physical quantities/variables are typeset in italic font (t for time, T for Temperature)
%%% Indices which are not defined are typeset in italic font (x, y, z, a, b, c)
%%% Items/objects which are defined are typeset in roman font (Car A, Car B)
%%% Descriptions/specifications which are defined by itself are typeset in roman font (abs, rel, ref, tot, net, ice)
%%% Abbreviations from 2 letters are typeset in roman font (RH, LAI)
%%% Vectors are identified in bold italic font using \vec{x}
%%% Matrices are identified in bold roman font
%%% Multiplication signs are typeset using the LaTeX commands \times (for vector products, grids, and exponential notations) or \cdot
%%% The character * should not be applied as mutliplication sign
%
%
%%% EQUATIONS
%
%%% Single-row equation
%
%\begin{equation}
%
%\end{equation}
%
%%% Multiline equation
%
%\begin{align}
%& 3 + 5 = 8\\
%& 3 + 5 = 8\\
%& 3 + 5 = 8
%\end{align}
%
%
%%% MATRICES
%
%\begin{matrix}
%x & y & z\\
%x & y & z\\
%x & y & z\\
%\end{matrix}
%
%
%%% ALGORITHM
%
%\begin{algorithm}
%\caption{...}
%\label{a1}
%\begin{algorithmic}
%...
%\end{algorithmic}
%\end{algorithm}
%
%
%%% CHEMICAL FORMULAS AND REACTIONS
%
%%% For formulas embedded in the text, please use \chem{}
%
%%% The reaction environment creates labels including the letter R, i.e. (R1), (R2), etc.
%
%\begin{reaction}
%%% \rightarrow should be used for normal (one-way) chemical reactions
%%% \rightleftharpoons should be used for equilibria
%%% \leftrightarrow should be used for resonance structures
%\end{reaction}
%
%
%%% PHYSICAL UNITS
%%%
%%% Please use \unit{} and apply the exponential notation


\end{document}
